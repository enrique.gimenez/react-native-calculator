import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {colors} from '../utils/colors';

interface ButtonProps {
  title: string;
  color?: string;
  textColor?: string;
  ancho?: boolean;
  onButtonPress: (numeroTexto: string) => void;
}

export const Button = ({
  title,
  color = colors.grisOscuro,
  textColor = 'white',
  ancho = false,
  onButtonPress,
}: ButtonProps) => {
  const {button, buttonText} = styles;
  return (
    <TouchableOpacity onPress={() => onButtonPress(title)}>
      <View style={[button, {backgroundColor: color, width: ancho ? 180 : 80}]}>
        <Text style={[buttonText, {color: textColor}]}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    height: 80,
    borderRadius: 100,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  buttonText: {
    textAlign: 'center',
    padding: 10,
    fontSize: 30,
    fontWeight: '400',
  },
});
