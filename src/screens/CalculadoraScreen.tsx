import {Text, View} from 'react-native';
import React from 'react';

import {Button} from '../components/Button';
import {styles} from '../theme/AppTheme';
import {colors} from '../utils/colors';
import {useCalculadora} from '../hooks/useCalculadora';

export const CalculadoraScreen = () => {
  const {
    limpiar,
    armarNumero,
    calcular,
    btnSumar,
    btnRestar,
    btnDividir,
    btnMultiplicar,
    btnDelete,
    positivoNegativo,
    numero,
    numeroAnterior,
  } = useCalculadora();

  const {resultado, resultadoSecundario, container, fila} = styles;
  const {grisClaro, naranja} = colors;

  return (
    <View style={container}>
      {numeroAnterior !== '0' && (
        <Text style={[resultado, resultadoSecundario]}>{numeroAnterior}</Text>
      )}
      <Text style={resultado} adjustsFontSizeToFit numberOfLines={1}>
        {numero}
      </Text>
      <View style={fila}>
        <Button
          title="C"
          color={grisClaro}
          textColor="black"
          onButtonPress={limpiar}
        />
        <Button
          title="+/-"
          color={grisClaro}
          textColor="black"
          onButtonPress={positivoNegativo}
        />
        <Button
          title="del"
          color={grisClaro}
          textColor="black"
          onButtonPress={btnDelete}
        />
        <Button title="/" color={naranja} onButtonPress={btnDividir} />
      </View>
      <View style={fila}>
        <Button title="7" onButtonPress={armarNumero} />
        <Button title="8" onButtonPress={armarNumero} />
        <Button title="9" onButtonPress={armarNumero} />
        <Button title="X" color={naranja} onButtonPress={btnMultiplicar} />
      </View>
      <View style={fila}>
        <Button title="4" onButtonPress={armarNumero} />
        <Button title="5" onButtonPress={armarNumero} />
        <Button title="6" onButtonPress={armarNumero} />
        <Button title="-" color={naranja} onButtonPress={btnRestar} />
      </View>
      <View style={fila}>
        <Button title="1" onButtonPress={armarNumero} />
        <Button title="2" onButtonPress={armarNumero} />
        <Button title="3" onButtonPress={armarNumero} />
        <Button title="+" color={naranja} onButtonPress={btnSumar} />
      </View>
      <View style={fila}>
        <Button title="0" ancho onButtonPress={armarNumero} />
        <Button title="." onButtonPress={armarNumero} />
        <Button title="=" color={naranja} onButtonPress={calcular} />
      </View>
    </View>
  );
};
