export const colors = {
  grisClaro: '#9b9b9b',
  grisOscuro: '#2d2d2d',
  grisSecundario: 'rgba(255, 255, 255, 0.5)',
  naranja: '#ff9427',
};
